<?php

namespace App\Repository;

use App\Entity\Tiponovedades;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Tiponovedades|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tiponovedades|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tiponovedades[]    findAll()
 * @method Tiponovedades[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TiponovedadesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tiponovedades::class);
    }
    public function getAllTipoNovedades(): QueryBuilder
    {
        return $this->createQueryBuilder('n');

    }

    // /**
    //  * @return Tiponovedades[] Returns an array of Tiponovedades objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tiponovedades
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
