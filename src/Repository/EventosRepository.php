<?php

namespace App\Repository;

use App\Entity\Eventos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Eventos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eventos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eventos[]    findAll()
 * @method Eventos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Eventos::class);
    }

    public function getAllEventos(UserInterface $user): QueryBuilder
    {
        return $this->createQueryBuilder('e')
            ->select('e,u')
            ->leftJoin('e.user', 'u')
            ->where('e.user is not null');
    }


}
