<?php

namespace App\Repository;

use App\Entity\OrdenesTrabajo;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method OrdenesTrabajo|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenesTrabajo|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenesTrabajo[]    findAll()
 * @method OrdenesTrabajo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenesTrabajoRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdenesTrabajo::class);
    }

    public function save(OrdenesTrabajo $ordenesTrabajo, bool $andFlush = true): void
    {
        $em = $this->getEntityManager();
        $em->persist($ordenesTrabajo);
        $andFlush && $em->flush();
    }

    public function getAllOrdersUserLogin(UserInterface|User $user): QueryBuilder
    {
        $query = $this->createQueryBuilder('o')
            ->select('o');
        !$user->isAdmin() && $query
            ->andWhere('o.user :user')
            ->setParameter('user', $user);
        return $query;

    }
}
