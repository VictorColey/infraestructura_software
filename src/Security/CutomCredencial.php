<?php

namespace App\Security;

use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Security\Core\Exception\LogicException;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CredentialsInterface;

class CutomCredencial implements CredentialsInterface
{

    private bool $valor;

    /**
     * @param bool $valor
     */
    public function __construct(bool $valor = true)
    {
        $this->valor = $valor;
    }


    /**
     * @param bool $valor
     */
    public function setValor(bool $valor): void
    {
        $this->valor = $valor;
    }


    public function isResolved(): bool
    {
        return $this->valor;
    }
}