<?php

namespace App\Controller;


use App\Entity\User;
use App\Form\ModuloUsuario\EditarUsuarioModuloType;
use App\Form\Usuario\EditarUsuarioType;
use App\Repository\UserRepository;
use App\Servicios\ModuloUsuario\Request\EditarUsuarioModulotRequest;
use App\Servicios\ModuloUsuario\UseCase\EditarUsuarioModuloUseCase;
use App\Servicios\ModuloUsuario\UseCase\EliminarEventoModuloUseCase;
use App\Servicios\ModuloUsuario\UseCase\EliminarUsuarioModuloUseCase;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/modulo/usuarios", name="modulo_usuarios")
 */
class ModuloUsuariosController extends AbstractController
{
    /**
     * @Route("/", name="_lista_usuarios")
     */
    public function index(
        Request            $request,
        PaginatorInterface $paginator,
        UserRepository     $userRepository
    ): Response
    {

        $userlog = $this->getUser();
        $allUsuariosQuery = $userRepository->getAllUsersNotUserLogin($userlog);
        $usuarios = $paginator->paginate(
            $allUsuariosQuery,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('panel/modulos/usuarios/lista_usuarios.html.twig', [
            'userlog' => $userlog,
            'pagination' => $usuarios
        ]);
    }
    /**
     * @Route("/{username}/editar", name="_editar")
     */
    public function editar(
        Request              $request,
        User                 $user,
        EditarUsuarioModuloUseCase $editarUsuarioUseCase
    )
    {
        $userlog = $this->getUser();
        $form = $this->createForm(EditarUsuarioModuloType::class, EditarUsuarioModulotRequest::fromUser($user));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $editarUsuarioUseCase->handled($user, $form->getData());
            return $this->redirectToRoute('modulo_usuarios_lista_usuarios');
        }
        return $this->render('panel/modulos/usuarios/editar_usuario.html.twig', [
            'form' => $form->createView(),
            'userlog' => $userlog
        ]);
    }


    /**
     * @Route("/{username}/eliminar", name="_eliminar")
     */
    public function elminiar(
        Request                $request,
        User                   $user,
        EliminarUsuarioModuloUseCase $eliminarUsuarioUseCase
    ): RedirectResponse
    {
        $eliminarUsuarioUseCase->handled($user);
        return $this->redirectToRoute('modulo_usuarios_lista_usuarios');
    }
}
