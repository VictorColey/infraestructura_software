<?php

namespace App\Controller;

use App\Entity\Eventos;
use App\Entity\User;
use App\Form\NotificacionEventos\NotificacionEventosType;
use App\Form\Usuario\EditarUsuarioType;
use App\Repository\EventosRepository;
use App\Servicios\NotificacionEventos\Request\NotificacionEventosRequest;
use App\Servicios\NotificacionEventos\UseCase\NotificacionEventosUseCase;
use App\Servicios\Usuario\Request\EditarUserRequest;
use App\Servicios\Usuario\UseCase\EditarUsuarioUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route(path="/notificacion", name="notificacion")
 */
class NotificacionEventosController extends AbstractController
{

    /**
     * @Route(path="/", name="_eventos")
     */
    public function new(
        Request $request,
        NotificacionEventosUseCase $notificacionEventosUseCase
    ): Response
    {
        $userlog = $this->getUser();
        $eventos = new Eventos();
        $eventos->setUser($userlog);
        $form = $this->createForm(NotificacionEventosType::class, NotificacionEventosRequest::fromEventos($eventos));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $notificacionEventosUseCase->handled($form->getData());

        }
        return $this->render('panel/notificaciones_eventos/notificacioners_eventos.html.twig', [
            'form' => $form->createView(),
            'userlog' => $userlog
        ]);
    }
}


