<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrarType\RegistrarType;
use App\Servicios\Registrar\RegistrarServicios;
use App\Servicios\Registrar\Request\RegistrarUserRequest;
use App\Servicios\Registrar\UseCase\RegisrarUsuarioUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/registrar", name="app_registrar")
     * @throws \Doctrine\ORM\ORMException
     */
    public function registrar(
        Request                $request,
        RegisrarUsuarioUseCase $regisrarUsuarioUseCase,
        RegistrarServicios     $registrarServicios
    ): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrarType::class,RegistrarUserRequest::fromUser($user));
        $form->handleRequest($request);
        $message = '';
        if ($form->isSubmitted() && $form->isValid()) {

            if($registrarServicios->isNotRegistrado($form['username']->getData())){
                $regisrarUsuarioUseCase->handled($form->getData());
                return $this->redirectToRoute('app_login');
            }else{
                $this->addFlash('fallo', 'el usuario ya esta registrado');
            }
        }
//        dd($form->isSubmitted(),$form->isValid() );
        return $this->render('security/registrar.html.twig',
            ['formRegistrar' => $form->createView(), 'message' => $message]);
    }


    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/inactivo", name="app_inactivo")
     */
    public function inactivo(Request $request): void
    {
        dd($request);

    }
}
