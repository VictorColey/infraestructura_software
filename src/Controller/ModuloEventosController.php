<?php

namespace App\Controller;

use App\Entity\Eventos;
use App\Repository\EventosRepository;
use App\Servicios\ModuloEventos\UseCase\EliminarEventoModuloUseCase;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/modulo/eventos", name="modulo_eventos")
 */
class ModuloEventosController extends AbstractController
{
    /**
     * @Route("/", name="_lista_eventos")
     */
    public function index(
        Request            $request,
        PaginatorInterface $paginator,
        EventosRepository $eventosRepository
    ): Response
    {

        $userlog = $this->getUser();
        $allEventosQuery = $eventosRepository->getAllEventos($userlog);
        $eventos = $paginator->paginate(
            $allEventosQuery,
            $request->query->getInt('page', 1),
            3
        );
        return $this->render('panel/modulos/usuarios/lista_eventos.html.twig', [
            'pagination' => $eventos,
            'userlog' => $userlog
        ]);

    }
    /**
     * @Route("/{id}/eliminar", name="_eliminar")
     */
    public function eliminar(
        Request                     $request,
        Eventos                     $eventos,
        EliminarEventoModuloUseCase $eliminarEventosUseCase
    ): RedirectResponse
    {
        $eliminarEventosUseCase->handled($eventos);
        return $this->redirectToRoute('modulo_eventos_lista_eventos');
    }
}