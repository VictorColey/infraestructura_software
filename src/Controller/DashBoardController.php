<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard", name="dash_board")
 */
class DashBoardController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     */
    public function index(Request $request): Response
    {
        $userlog = $this->getUser();
        return $this->render('panel/dashboard.html.twig', [
            'userlog' => $userlog
        ]);
    }
}