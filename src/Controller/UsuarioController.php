<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Usuario\EditarUsuarioType;
use App\Servicios\Usuario\Request\EditarUserRequest;
use App\Servicios\Usuario\UseCase\EditarUsuarioUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/usuario", name="usuario")
 */
class UsuarioController extends AbstractController
{
    /**
     * @Route("/", name="_lista")
     */
    public function lista()
    {

    }

    /**
     * @Route("/{id}/editar", name="_editar")
     */
    public function editar(
        Request              $request,
        User                 $user,
        EditarUsuarioUseCase $editarUsuarioUseCase
    )
    {

        $userlog = $this->getUser();
        $form = $this->createForm(EditarUsuarioType::class, EditarUserRequest::fromUser($user));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $editarUsuarioUseCase->handled($user, $form->getData());
            return $this->redirectToRoute('dash_board_index');
        }
        return $this->render('panel/usuario/content_editar_usuario.html.twig', [
            'form' => $form->createView(),
            'userlog' => $userlog
        ]);
    }




}