<?php

namespace App\Controller;

use App\Entity\Eventos;
use App\Entity\OrdenesTrabajo;
use App\Form\ModuloOrdenTrabajo\OrdenTrebajoType;
use App\Repository\OrdenesTrabajoRepository;
use App\Servicios\ModuloOrdenesTrabajo\CaseUse\CrearOrdenTrabajoUseCase;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/ordenes/trabajo", name="ordenes_trabajo_")
 */
class OrdenesTrabajoController extends AbstractController
{
    /**
     * @Route(path="/", name="list")
     */
    public function index(
        Request                  $request,
        PaginatorInterface       $paginator,
        OrdenesTrabajoRepository $ordenesTrabajoRepository
    ): Response
    {
        $userlog = $this->getUser();
        $allUsuariosQuery = $ordenesTrabajoRepository->getAllOrdersUserLogin($userlog);
        $ordenesTrabajo = $paginator->paginate(
            $allUsuariosQuery,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('panel/ordenes_trabajo/index.html.twig', [
            'userlog' => $userlog,
            'pagination' => $ordenesTrabajo
        ]);
    }

    /**
     * @Route("/{id}/crear", name="crear")
     */
    public function new(
        Request                  $request,
        Eventos                  $eventos,
        CrearOrdenTrabajoUseCase $crearOrdenTrabajoUseCase
    ): Response
    {

        $userlog = $this->getUser();
        $form = $this->createForm(OrdenTrebajoType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $crearOrdenTrabajoUseCase->handled($form->getData(),$eventos);
            return $this->redirectToRoute('dash_board_index');
        }
        return $this->render('panel/ordenes_trabajo/Form/crear.html.twig', [
            'form' => $form->createView(),
            'userlog' => $userlog,
            'eventos' => $eventos
        ]);
    }

}
