<?php

namespace App\Form\ModuloOrdenTrabajo;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Servicios\ModuloOrdenesTrabajo\Request\OrdenesTrabajoRequest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class OrdenTrebajoType extends AbstractType
{
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class,
                [
                    'query_builder' => function (UserRepository $repository) {
                        $query = $repository->createQueryBuilder('u')

                        ;
                    },
                    'class' => User::class,
                    'choice_label' => 'nombre'
                ]
            )
            ->add('start_at', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('end_at', DateType::class, [
                'widget' => 'single_text',
            ]);
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrdenesTrabajoRequest::class,
        ]);
    }
}