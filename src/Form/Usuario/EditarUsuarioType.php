<?php

namespace App\Form\Usuario;


use App\Entity\Cargos;
use App\Repository\CargosRepository;
use App\Servicios\Usuario\Request\EditarUserRequest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditarUsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre', TextType::class)
            ->add('apellido', TextType::class)
            ->add('tipo_documento', ChoiceType::class, [
                'choices' => [
                    'Cédula de ciudadanía' => 'CC',
                    'Tarjeta de identidad' => 'TI',
                    'Cédula de extranjería' => 'CE',
                    'Contraseña registraduría' => 'CR',
                    'Pasaporte Colombiano' => 'PC',
                    'Pasaporte extranjero' => 'PE'
                ],
            ])
            ->add('telefono', TextType::class)
            ->add('correo', TextType::class)
            ->add('cargo', EntityType::class, [
                'class' => Cargos::class,
                'query_builder' => fn (CargosRepository $cargosRepository) =>$cargosRepository->getAllCargos(),
                'choice_label' => 'nombre',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EditarUserRequest::class,
        ]);
    }
}