<?php

namespace App\Form\NotificacionEventos;

use App\Entity\Tiponovedades;
use App\Repository\TiponovedadesRepository;
use App\Servicios\NotificacionEventos\Request\NotificacionEventosRequest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class NotificacionEventosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fecha', DateTimeType::class, [
                'time_label' => 'Starts On',
            ])
            ->add('detalle', TextType::class)
            ->add('image', FileType::class,[
                'label'=>'archivo en jpg'
            ])
        ;

    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NotificacionEventosRequest::class,
        ]);
    }
}