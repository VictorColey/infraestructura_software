<?php

namespace App\Entity;

use App\Repository\EventosRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity(repositoryClass=EventosRepository::class)
 */
class Eventos
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(name= "detalle", type= "string", length= 255, nullable= true)
     */
    private $detalle;



    /**
     * @ORM\Column(name= "fecha", type= "datetime", length= 255, nullable= true)
     */
    private $fecha;


    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $user;


    /**
     * @ORM\Column(type= "string", length= 255)
     */
    private $image;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetalle(): ?string
    {
        return $this->detalle;
    }

    public function setDetalle(?string $detalle): self
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

}


