<?php

namespace App\Entity;


use App\Repository\OrdenesTrabajoRepository;
use App\Servicios\ModuloOrdenesTrabajo\Request\OrdenesTrabajoRequest;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;


//#[ORM\Table('ordenes_trabajo')]
/**
 * @ORM\Entity(repositoryClass=OrdenesTrabajoRepository::class)
 */
class OrdenesTrabajo
{

    const ORDER_PENDIENTE = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private  $id;

    /**
     * @ORM\ManyToOne(targetEntity=Eventos::class)
     */
    private  $evento;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable= false)
     */
    private  $user;

    /**
     * @ORM\Column(type= "datetime")
     */
    private  $end_at;

    /**
     * @ORM\Column(type= "datetime")
     */
    private  $start_at;

    /**
     * @ORM\Column(type= "string", length= 255)
     */
    private  $numero_orden;

    /**
     * @ORM\Column(type= "integer")
     */
    private  $status_orden = self::ORDER_PENDIENTE;

    private $test = 1;

//    public function __construct(
//        User      $user,
//        \DateTime $end_at,
//        \DateTime $start_at,
//        Eventos   $evento
//    )
//    {
//        $this->user = $user;
//        $this->end_at = $end_at;
//        $this->start_at = $start_at;
//        $this->evento = $evento;
//
//    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvento(): ?Eventos
    {
        return $this->evento;
    }

    public function setEvento(?Eventos $evento): self
    {
        $this->evento = $evento;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEndAt(): ?\DateTime
    {
        return $this->end_at;
    }

    public function setEndAt(\DateTime $end_at): self
    {
        $this->end_at = $end_at;

        return $this;
    }

    public function getStartAt(): ?\DateTime
    {
        return $this->start_at;
    }

    public function setStartAt(\DateTime $start_at): self
    {
        $this->start_at = $start_at;

        return $this;
    }

    public function getNumeroOrden(): ?string
    {
        return $this->numero_orden;
    }

    public function setNumeroOrden(string $numero_orden): self
    {
        $this->numero_orden = $numero_orden;

        return $this;
    }

    public function getStatusOrden(): ?int
    {
        return $this->status_orden;
    }

    public function setStatusOrden(int $status_orden): self
    {
        $this->status_orden = $status_orden;

        return $this;
    }

//    #[ORM\PrePersist]
//    public function MarkAsPendient()
//    {
//        $this->status_orden = self::ORDER_PENDIENTE;
//    }
//
//    #[Pure] public static function createFromRequest(OrdenesTrabajoRequest $request, Eventos $eventos): OrdenesTrabajo
//    {
//        return new static(
//            $request->user,
//            $request->end_at,
//            $request->start_at,
//            $eventos
//        );
//    }

}
