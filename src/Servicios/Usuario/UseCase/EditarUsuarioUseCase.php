<?php

namespace App\Servicios\Usuario\UseCase;

use App\Entity\User;
use App\Servicios\Usuario\Request\EditarUserRequest;
use Doctrine\ORM\EntityManagerInterface;


class EditarUsuarioUseCase
{
    private EntityManagerInterface $entityManager;


    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function handled(User $user,EditarUserRequest $userRequest)
    {
        $user->setNombre($userRequest->nombre);
        $user->setApellido($userRequest->apellido);
        $user->setTipoDocumento($userRequest->tipo_documento);
        $user->setTelefono($userRequest->telefono);
        $user->setCorreo($userRequest->correo);
        $user->setCargo($userRequest->cargo);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}