<?php

namespace App\Servicios\ModuloUsuario\Request;

use App\Entity\Cargos;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class EditarUsuarioModulotRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     */
    public $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     */
    public $apellido;

    /**
     * @var string
     */
    public $tipo_documento;

    /**
     * @var string
     */
    public $rol;

    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     */
    public $telefono;

    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     * @Assert\Email(message="Debe de ser un correo valido")
     */
    public $correo;

    /**
     * @var Cargos
     */
    public $cargo;


    /**
     * @return static
     */
    public static function fromUser(User $user): self
    {
        $request = new static();
        $request->nombre = $user->getNombre();
        $request->apellido = $user->getApellido();
        $request->tipo_documento = $user->getTipoDocumento();
        $request->rol = $user->getRol();
        $request->telefono = $user->getTelefono();
        $request->correo = $user->getCorreo();
        $request->cargo = $user->getCargo();
        return $request;
    }
}