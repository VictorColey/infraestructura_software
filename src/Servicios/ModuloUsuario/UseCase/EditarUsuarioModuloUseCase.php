<?php

namespace App\Servicios\ModuloUsuario\UseCase;

use App\Entity\User;
use App\Servicios\ModuloUsuario\Request\EditarUsuarioModulotRequest;
use App\Servicios\Usuario\Request\EditarUserRequest;
use Doctrine\ORM\EntityManagerInterface;


class EditarUsuarioModuloUseCase
{
    private EntityManagerInterface $entityManager;


    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function handled(User $user,EditarUsuarioModulotRequest $userRequest)
    {
        $user->setNombre($userRequest->nombre);
        $user->setApellido($userRequest->apellido);
        $user->setTipoDocumento($userRequest->tipo_documento);
        $user->setTelefono($userRequest->telefono);
        $user->setCorreo($userRequest->correo);
        $user->setCargo($userRequest->cargo);
        $user->setRol($userRequest->rol);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}