<?php

namespace App\Servicios\ModuloUsuario\UseCase;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class EliminarUsuarioModuloUseCase
{

    private EntityManagerInterface $entityManager;


    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function handled(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}