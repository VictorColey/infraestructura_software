<?php

namespace App\Servicios\Registrar;

use App\Entity\User;
use App\Repository\UserRepository;
use function PHPUnit\Framework\isEmpty;

class RegistrarServicios
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function isNotRegistrado(string $username): bool
    {
        return $this->userRepository->findBy(['username'=>$username]) == null;
    }

}