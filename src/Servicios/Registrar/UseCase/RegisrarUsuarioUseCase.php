<?php

namespace App\Servicios\Registrar\UseCase;

use App\Entity\User;
use App\Repository\TmRolesRepository;
use App\Repository\UserRepository;
use App\Servicios\Registrar\Request\RegistrarUserRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisrarUsuarioUseCase
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;


    const ROL_USER = 5;

    public function __construct(
        EntityManagerInterface       $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;


    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function handled(RegistrarUserRequest $registrarRequest): void
    {
        $usuario = new User();
        $usuario->setUsername($registrarRequest->username);
        $usuario->setNombre($registrarRequest->nombre);
        $usuario->setApellido($registrarRequest->apellido);
        $usuario->setTipoDocumento($registrarRequest->tipodocumento);
        $usuario->setTelefono($registrarRequest->telefono);
        $usuario->setCorreo($registrarRequest->correo);
        $usuario->setCargo($registrarRequest->cargo);
        $usuario->setPassword($this->passwordEncoder->encodePassword($usuario, $registrarRequest->password));
        $usuario->setRoles(['ROLE_SENA']);
        $usuario->activarStatus();
        $this->entityManager->persist($usuario);
        $this->entityManager->flush();;
   }
}