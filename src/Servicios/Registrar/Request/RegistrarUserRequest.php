<?php

namespace App\Servicios\Registrar\Request;

use App\Entity\Cargos;
use App\Entity\User;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RegistrarUserRequest
{
    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     * @Assert\Regex("/^[0-9]+$/",message="debe de ser solo numero")
     */
    public $username;

    /**
     * @var string
     */
    public $nombre;

    /**
     * @var string
     */
    public $apellido;

    /**
     * @var string
     */
    public $tipodocumento;

    /**
     * @var string
     */
    public $telefono;

    /**
     * @var string
     * @Assert\NotBlank(message="no debe de ser vacio")
     * @Assert\Email(message="Debe de ser un correo valido")
     */
    public $correo;

    /**
     * @var Cargos
     */
    public $cargo;

    /**
     * @var PasswordAuthenticatedUserInterface
     */
    public $password;

    /**
     * @return static
     */
    public static function fromUser(User $user): self
    {
        $request = new static();
        $request->username = $user->getUsername();
        $request->password = "123";
        $request->nombre = $user->getNombre();
        $request->apellido = $user->getApellido();
        $request->tipodocumento = $user->getTipoDocumento();
        $request->telefono = $user->getTelefono();
        $request->correo = $user->getCorreo();
        $request->cargo = $user->getCargo();
        return $request;
    }


}