<?php

namespace App\Servicios\ModuloOrdenesTrabajo\CaseUse;

use App\Entity\Eventos;
use App\Entity\OrdenesTrabajo;
use App\Repository\OrdenesTrabajoRepository;
use App\Servicios\ModuloOrdenesTrabajo\Request\OrdenesTrabajoRequest;

class CrearOrdenTrabajoUseCase
{

    public function __construct(private OrdenesTrabajoRepository $clientRepository)
    {
    }

    public function handle(OrdenesTrabajoRequest $request, Eventos $eventos): OrdenesTrabajo
    {
        $ordenesTrabajo = OrdenesTrabajo::createFromRequest($request,$eventos);
        $this->clientRepository->save($ordenesTrabajo);
        return $ordenesTrabajo;
    }

}