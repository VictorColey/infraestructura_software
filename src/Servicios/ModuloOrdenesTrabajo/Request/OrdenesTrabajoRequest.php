<?php

namespace App\Servicios\ModuloOrdenesTrabajo\Request;

use App\Entity\OrdenesTrabajo;
use App\Entity\User;
use JetBrains\PhpStorm\Pure;

class OrdenesTrabajoRequest
{


    public OrdenesTrabajo $ordenesTrabajo;

    public User $user;

    public \DateTime $end_at;

    public \DateTime $start_at;

    #[Pure] public static function fromOrdenTrabajo(OrdenesTrabajo $ordenesTrabajo): self
    {
        $request = new static();
        $request->ordenesTrabajo = $ordenesTrabajo;
        $request->user = $ordenesTrabajo->getUser();
        $request->start_at = $ordenesTrabajo->getStartAt();
        $request->end_at = $ordenesTrabajo->getEndAt();
        return $request;
    }
}