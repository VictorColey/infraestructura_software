<?php

namespace App\Servicios\NotificacionEventos\Request;

use App\Entity\Eventos;
use App\Entity\Tiponovedades;
use App\Entity\User;
use App\Servicios\ModuloUsuario\Request\EditarUsuarioModulotRequest;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * * @Vich\Uploadable
 */
class NotificacionEventosRequest
{

    /**
     * @var string
     */
    public $detalle;
    /**
     * @var User
     */
    public $user;
    /**
     * @Assert\NotBlank(message="Por favor inserta tu imagen")
     * @Assert\File(mimeTypes={"image/jpeg"})
     */
    public $image;

    /**
     * @var \DateTime
     */
    public $fecha;
    /**
     * @var Tiponovedades
     */
    public $tiponovedades;


    /**
     * @return static
     */
    public static function fromEventos(Eventos $eventos): self
    {

        $request = new static();
        $request->detalle = $eventos->getDetalle();
        $request->fecha = $eventos->getFecha();
        $request->image = $eventos->getImage();
        $request->user = $eventos->getUser();
        return $request;
    }
}
