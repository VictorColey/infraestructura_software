<?php

namespace App\Servicios\NotificacionEventos\UseCase;

use App\Entity\Eventos;
use App\Servicios\NotificacionEventos\Request\NotificacionEventosRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NotificacionEventosUseCase
{
    private EntityManagerInterface $entityManager;


    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function handled( NotificacionEventosRequest $notificacionEventosRequest): void
    {
        /** @var UploadedFile $file */
        $file = $notificacionEventosRequest->image;
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $cvDir = '../public/images/products';
        $file->move($cvDir, $fileName);
        $eventos = new Eventos();
        $eventos->setImage($fileName);
        $eventos->setDetalle($notificacionEventosRequest->detalle);
        $eventos->setFecha($notificacionEventosRequest->fecha);
        $eventos->setUser($notificacionEventosRequest->user);

        $this->entityManager->persist($eventos);
        $this->entityManager->flush();
    }
}