<?php

namespace App\Servicios\ModuloEventos\UseCase;

use App\Entity\Eventos;
use Doctrine\ORM\EntityManagerInterface;

class EliminarEventoModuloUseCase
{

    private EntityManagerInterface $entityManager;


    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function handled(Eventos $eventos)
    {
        $this->entityManager->remove($eventos);
        $this->entityManager->flush();
    }
}