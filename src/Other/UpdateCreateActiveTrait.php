<?php


namespace App\Other;


use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;


trait UpdateCreateActiveTrait
{


    /**
     * @ORM\Column(type="datetime",  name="createdat")
     */
    private $createdat;

    /**
     * @ORM\Column(type="datetime",  name="updatedat")
     */
    private $updatedat;

    /**
     * @ORM\Column(type="integer",  name="statusid")
     */
    private $statusid;

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedat(): ?DateTimeInterface
    {
        return $this->createdat;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedat(): void
    {
        $this->createdat = new DateTime();

    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedat(): ?DateTimeInterface
    {
        return $this->updatedat;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setUpdatedat(): void
    {
        $this->updatedat = new DateTime();
    }

    /**
     * @return integer
     */
    public function getStatusid(): ?int
    {
        return $this->statusid;
    }


    public function setStatusid(int $statusid): self
    {
        $this->statusid = $statusid;
        return $this;
    }

    public function activarStatus(): void
    {
        $this->statusid = 1;
    }

    public function desactivarStatus(): void
    {
        $this->statusid = 2;
    }
}