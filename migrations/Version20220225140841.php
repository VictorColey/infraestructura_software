<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220225140841 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE eventos ADD image_size INT NOT NULL, CHANGE image image_name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE num_documento num_documento VARCHAR(20) NOT NULL, CHANGE password password VARCHAR(255) NOT NULL, CHANGE createdat createdat DATETIME NOT NULL, CHANGE updatedat updatedat DATETIME NOT NULL, CHANGE statusid statusid INT NOT NULL, CHANGE nombre nombre VARCHAR(255) NOT NULL, CHANGE telefono telefono VARCHAR(255) NOT NULL, CHANGE apellido apellido VARCHAR(255) NOT NULL, CHANGE correo correo VARCHAR(255) NOT NULL, CHANGE tipo_documento tipo_documento VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX IDX_8D93D649813AC380 ON user (cargo_id)');
        $this->addSql('ALTER TABLE user RENAME INDEX cedula TO UNIQ_8D93D649C46F5A1B');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cargos CHANGE nombre nombre VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE eventos DROP FOREIGN KEY FK_6B23BD8FA76ED395');
        $this->addSql('ALTER TABLE eventos ADD image VARCHAR(255) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, DROP image_name, DROP image_size, CHANGE detalle detalle VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649813AC380');
        $this->addSql('DROP INDEX IDX_8D93D649813AC380 ON user');
        $this->addSql('ALTER TABLE user CHANGE num_documento num_documento VARCHAR(20) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE nombre nombre VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE tipo_documento tipo_documento VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE apellido apellido VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE telefono telefono VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE correo correo VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE createdat createdat DATETIME DEFAULT NULL, CHANGE updatedat updatedat DATETIME DEFAULT NULL, CHANGE statusid statusid INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d649c46f5a1b TO cedula');
    }
}
